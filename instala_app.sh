#!/bin/bash
#VARIABLES A MODIFICAR
DOMINIO='example.com'
DOMINIO2='www.example.com'
IP='10.10.1.4'
IPBAL='10.10.1.3'
RUTA=$PWD
#INSTALACION DE WORDPRESS+APACHE+MYSQL
sudo apt update -y 
sudo apt upgrade -y 
sudo apt install apache2 -y 
sudo apt install mysql-server -y
sudo apt install php libapache2-mod-php php-mysql -y
sudo apt install php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip -y
sudo apt install iptraf-ng
sudo apt install software-properties-common && sudo add-apt-repository ppa:ondrej/php -y
sudo apt install php-imagick
sudo apt install php7.4-imagick
sudo apt install php8.0-imagick
echo "extension=imagick" >> /etc/php/7.4/apache2/php.ini 
echo "$IPBAL   $DOMINIO
$IPBAL $DOMINIO2" >> /etc/hosts
sudo echo "$DOMINIO" > /etc/hostname
sudo mkdir /var/www/$DOMINIO
sudo chown -R $USER:$USER /var/www/$DOMINIO/
sudo touch /etc/apache2/sites-available/$DOMINIO.conf

echo "<VirtualHost *:80>
    ServerName $DOMINIO
    ServerAlias www.$DOMINIO
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/$DOMINIO
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    <Directory /var/www/$DOMINIO/>
    AllowOverride All
    </Directory>

</VirtualHost> " > /etc/apache2/sites-available/$DOMINIO.conf

sudo a2ensite $DOMINIO
sudo a2dissite 000-default.conf
sudo a2enmod ssl
cd /tmp 
curl -O https://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
touch /tmp/wordpress/.htaccess
cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php
mkdir /tmp/wordpress/wp-content/upgrade
sudo cp -a /tmp/wordpress/. /var/www/$DOMINIO/
sudo chown -R www-data:www-data /var/www/$DOMINIO/
sudo sed -i 's/username_here/u_wordpress/g' "/var/www/$DOMINIO/wp-config.php"
sudo sed -i 's/password_here/Wr%q#@vRpaJC/g' "/var/www/$DOMINIO/wp-config.php"
sudo sed -i 's/database_name_here/db_wordpress/g' "/var/www/$DOMINIO/wp-config.php"
sudo find /var/www/$DOMINIO/ -type d -exec chmod 750 {} \;
sudo find /var/www/$DOMINIO/ -type f -exec chmod 640 {} \;
sudo a2enmod rewrite
sudo mysql < $RUTA/instala_bd.sql
sudo systemctl reload apache2
sudo sed -i "s/10.10.1.254/${IP}/g" "/etc/netplan/00-installer-config.yaml"
sudo reboot

####### Modificar en el archivo wp-config.php ################ 

#define('FORCE_SSL_ADMIN', true);
#define('WP_HOME','https://test2.pnsu.gob.pe');
#define('WP_SITEURL','https://test2.pnsu.gob.pe');
#if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
#       $_SERVER['HTTPS']='on';

# REFERENCIA: https://superadmin.es/blog/devops/proxy-inverso-wordpress-nginx/ 
# REFERENCIA: https://www.linuxcapable.com/how-to-install-php-imagemagick-imagick-on-ubuntu-20-04/ 

