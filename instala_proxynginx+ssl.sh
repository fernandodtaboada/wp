#!/bin/bash
DOMINIO1='example.com'
IPBAL='10.10.1.3'
HOSTNAME1='bal'
RUTA=$PWD

sudo apt  update
sudo apt apt upgrade
sudo apt install nginx
sudo touch  /etc/nginx/sites-available/$DOMINIO1.conf
sudo echo "$IPBAL   $DOMINIO1" >> /etc/hosts
sudo echo "$HOSTNAME1" > /etc/hostname

echo "server {
    listen      80;
    server_name $DOMINIO1  www.$DOMINIO1;
        location / {
        proxy_pass http://$IPBAL:80;proxy_http_version                 1.1;
        proxy_cache_bypass                "'$http_upgrade'";

        # Proxy headers
        proxy_set_header Upgrade           "'$http_upgrade'";
        proxy_set_header Connection        "upgrade";
        proxy_set_header Host              "'$host'";
        proxy_set_header X-Real-IP         "'$remote_addr'";
        proxy_set_header X-Forwarded-For   "'$proxy_add_x_forwarded_for'";
        proxy_set_header X-Forwarded-Proto "'$scheme'";
        proxy_set_header X-Forwarded-Host  "'$host'";
        proxy_set_header X-Forwarded-Port  "'$server_port'";

        # Proxy timeouts
        proxy_connect_timeout              60s;
        proxy_send_timeout                 60s;
        proxy_read_timeout                 60s;
        }

}" >  /etc/nginx/sites-available/$DOMINIO1.conf

sudo ln -s /etc/nginx/sites-available/$DOMINIO1.conf /etc/nginx/sites-enabled/
sudo systemctl reload nginx

#INSTALANDO CERTIFICADO SSL EN EL PROXY REVERSO
sudo apt install certbot python3-certbot-nginx -y
sudo certbot --nginx --agree-tos -d $DOMINIO1 -d www.$DOMINIO1 -m fernando.taboada@gmail.com  --no-eff-email --redirect
#sudo systemctl status certbot.timer
#sudo certbot renew --dry-run
sudo systemctl reload nginx

#REFERENCIA : https://superadmin.es/blog/devops/proxy-inverso-wordpress-nginx/
#https://howtoforge.es/como-instalar-wordpress-con-nginx-y-let-s-encrypt-ssl-en-ubuntu-22-04/ 
